
    <?php include('partials-front/menu.php'); ?>

    <!-- Book sEARCH Section Starts Here -->
    <section class="book-search text-center">
        <div class="container">
            <?php 

                //Get the Search Keyword
                $search = $_POST['search'];
            
            ?>


            <h2>Books on Your Search <a href="#" class="text-white">"<?php echo $search; ?>"</a></h2>

        </div>
    </section>
    <!-- Book sEARCH Section Ends Here -->



    <!-- Book MEnu Section Starts Here -->
    <section class="book-menu">
        <div class="container">
            <h2 class="text-center">Books Menu</h2>

            <?php 

                //SQL Query to Get books based on search keyword
                $sql = "SELECT * FROM tbl_book WHERE title LIKE '%$search%' OR description LIKE '%$search%'";

                //Execute the Query
                $res = mysqli_query($conn, $sql);

                //Count Rows
                $count = mysqli_num_rows($res);

                //Check whether book available of not
                if($count>0)
                {
                    //Book Available
                    while($row=mysqli_fetch_assoc($res))
                    {
                        //Get the details
                        $id = $row['id'];
                        $title = $row['title'];
                        $price = $row['price'];
                        $description = $row['description'];
                        $image_name = $row['image_name'];
                        ?>

                        <div class="book-menu-box">
                            <div class="book-menu-img">
                                <?php 
                                    // Check whether image name is available or not
                                    if($image_name=="")
                                    {
                                        //Image not Available
                                        echo "<div class='error'>Image not Available.</div>";
                                    }
                                    else
                                    {
                                        //Image Available
                                        ?>
                                        <img src="<?php echo SITEURL; ?>images/<?php echo $image_name; ?>" alt="Book" class="img-responsive img-curve">
                                        <?php 

                                    }
                                ?>
                                
                            </div>

                            <div class="book-menu-desc">
                                <h4><?php echo $title; ?></h4>
                                <p class="book-price">$<?php echo $price; ?></p>
                                <p class="book-detail">
                                    <?php echo $description; ?>
                                </p>
                                <br>

                                <a href="#" class="btn btn-primary">Order Now</a>
                            </div>
                        </div>

                        <?php
                    }
                }
                else
                {
                    //Book Not Available
                    echo "<div class='error'>Book not found.</div>";
                }
            
            ?>

            

            <div class="clearfix"></div>

            

        </div>

    </section>
    <!-- Book Menu Section Ends Here -->

    <?php include('partials-front/footer.php'); ?>